FROM ikuturso/event:latest

COPY ./entrypoint.sh /entrypoint.sh
COPY ./alerta.conf /root/.alerta.conf

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
