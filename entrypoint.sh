#!/bin/sh
until wget -q -s http://alerta 2>/dev/null;
do
    echo wait for alerta...
    sleep 5
done

exec alerta send -r web01 -e NodeUp -E Production -S Website -s major -t 'SIEMonster is UP.' -v AWESOME
